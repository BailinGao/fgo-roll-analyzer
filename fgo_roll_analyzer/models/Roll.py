from dataclasses import dataclass
from . import RollType as Type

@dataclass
class Roll:
    """Represents a single roll of the gacha"""

    type: Type