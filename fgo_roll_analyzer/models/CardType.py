from enum import Enum

class CardType(Enum):
    CRAFT_ESSENSE = 1
    SERVANT = 2