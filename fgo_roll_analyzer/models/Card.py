from dataclasses import dataclass
from . import CardRarity as Rarity
from . import CardType as Type

@dataclass
class Card:
    rarity: Rarity
    type: Type
    rateUp: bool = False