from enum import Enum

class RollType(Enum):
    SINGLE = 1
    MULTI = 2