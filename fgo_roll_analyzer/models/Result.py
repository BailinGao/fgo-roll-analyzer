from dataclasses import dataclass
from typing import List
from .Card import Card


@dataclass
class Result:
    """The result of a ~Roll"""

    items: List[Card]