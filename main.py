from fgo_roll_analyzer.models.Roll import Roll
from fgo_roll_analyzer.models.RollType import RollType
from fgo_roll_analyzer.service.ResultAnalyzer import analyze
from fgo_roll_analyzer.models.Result import Result
from fgo_roll_analyzer.models.Card import Card
from fgo_roll_analyzer.models.CardRarity import CardRarity
from fgo_roll_analyzer.models.CardType import CardType

result = Result([Card(CardRarity.FiveStar, CardType.SERVANT)])

print(analyze(result))