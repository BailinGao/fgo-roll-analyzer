import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
    
setuptools.setup(
    name="fgo-roll-analyzer",
    version="0.0.1",
    author="Bailin Gao",
    author_email="BailinGao@gmail.com",
    description="Library that calculates the likelihood of getting a certain result with Fate Grand Order",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/BailinGao/fgo-roll-analyzer",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)